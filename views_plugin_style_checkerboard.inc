<?php

/**
 * @file
 * Contains the Checkerboard style plugin.
 */

/**
 * Default style plugin to render a checkerboard page.
 *
 * @ingroup views_style_plugins
 */
class views_plugin_style_checkerboard extends views_plugin_style {
  function uses_fields() { return TRUE; }
  
  function option_definition() {
    $options = parent::option_definition();
    $options['checkerboard']['row_field'] = array('default' => '');
    $options['checkerboard']['col_field'] = array('default' => '');
    $options['checkerboard']['title'] = array('default' => '');
    $options['checkerboard']['value_key'] = array('default' => 'target_id');

    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $options = array('' => t('- None -'));
    $field_labels = $this->display->handler->get_field_labels(TRUE);
    $options += $field_labels;
    // If there are no fields, we can't group on them.
    $form['checkerboard']['row_field'] = array(
      '#type' => 'select',
      '#title' => t('Select row field'),
      '#options' => $options,
      '#default_value' => $this->options['checkerboard']['row_field'],
      '#description' => t('Select a field to use for the row.'),
    );
    $form['checkerboard']['col_field'] = array(
      '#type' => 'select',
      '#title' => t('Select column field'),
      '#options' => $options,
      '#default_value' => $this->options['checkerboard']['col_field'],
      '#description' => t('Select a field to use for the column.'),
    );
    $form['checkerboard']['title'] = array(
      '#type' => 'textfield',
      '#title' => 'Default content title',
      '#default_value' => $this->options['checkerboard']['title'],
      '#description' => t('Set default text for content title.'),
    );
    $form['checkerboard']['value_key'] = array(
      '#type' => 'textfield',
      '#title' => 'Default value key',
      '#default_value' => $this->options['checkerboard']['value_key'],
      '#description' => t('Set default value key.'),
    );
    
    // unset($form['grouping']);
  }
  
  function render() {
    if (isset($this->options['checkerboard']['row_field']) && isset($this->options['checkerboard']['col_field'])) {
      $row_field = $this->options['checkerboard']['row_field'];
      $col_field = $this->options['checkerboard']['col_field'];
      $value_key = $this->options['checkerboard']['value_key'];

      $field = field_info_field($row_field);
      $rows = entityreference_options_list($field);
      $field = field_info_field($col_field);
      $cols = entityreference_options_list($field);

      $data = array();
      foreach ($this->view->result as $index => $value) {
        $row_value = $this->get_field_value($index, $row_field);
        $rid = $row_value[0][$value_key];
        $col_value = $this->get_field_value($index, $col_field);
        $cid = $col_value[0][$value_key];
        $data[$rid][$cid]['value'] = 1;
        $data[$rid][$cid]['index'] = $index;
        $data[$rid][$cid]['nid'] = $value->nid;
      }
      
      return drupal_get_form('views_checkerboard_form_', $rows, $cols, $data, $this->options['checkerboard']);
    }
    return '';
  }
}
