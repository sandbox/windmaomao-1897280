<?php
/**
 * @file
 * Contains the page display plugin.
 */

/**
 * The plugin that handles a sparse callback.
 *
 * @ingroup views_display_plugins
 */
class views_plugin_display_sparse extends views_plugin_display {

  /**
   * The page display has a path.
   */
  // function has_path() { return TRUE; }
  // function uses_breadcrumb() { return TRUE; }
  function get_style_type() { return 'sparse'; }

  function option_definition() {
    $options = parent::option_definition();
    
    $options['style_plugin']['default'] = 'checkerboard';
    $options['style_options']['default']  = array('description' => '');
    $options['row_plugin']['default'] = '';
    $options['defaults']['default']['style_plugin'] = FALSE;
    $options['defaults']['default']['style_options'] = FALSE;
    $options['defaults']['default']['row_plugin'] = FALSE;
    $options['defaults']['default']['row_options'] = FALSE;

    return $options;
  }
  
  function render() {
    return $this->view->style_plugin->render($this->view->result);
  }

}
